# Development test

## User story
As a student

I want to see the program information on relevant program details page,

So I can learn more about the program.

## Acceptance Criteria
* A Drupal 7 module which meets the Drupal coding and best paractise standards.
* Display program code, program title, program level, program merit and faculty
  on a program details page.
* Program detail page can be accessed via /program/{code}.
* Display a 404 page if the requested program data is not found.
* If program merits is a numeric value, display it with a percentage sign
  (e.g. 0.156%). If it is not a numeric value, display the text as is.
* Display data in a table format (see attached wireframe).
* PHP unit test is required.
* Styling and thememing are desired but not required.
* Document anything you think needs documentation and any assumptions you've
  made.
* Document reasons if you have not followed the structure of scope of work.

## Wireframe

![picture](img/wireframe.png)

## Skills and Abilities You Might Demonstrate
* Git: commit often to view your work progress
* Problem solving and attention to detail: ability to work with the scope of
  work
* Development skills: UI design, back end & front end development, PHP, Drupal,
  HTML, JavaScript, automated tests, CSS, SCSS.

## Scope of work

### Modules:
* uq_example

### Components:
* Drupal\uq_example\Entity\Program class
    * uq_example/src/Entity/Program.php
    * Program is responsible for program information.
* Drupal\uq_example\Factory\ProgramFactory class
    * uq_example/src/Factory/ProgramFactory.php
    * ProgramFactory is responsible for creating Program entity objects.
    * method createProgram($code) returns a Drupal\uq_example\Entity\Program
      object or throws an InvalidArgumentException if the code isn't recognised.
        * Sample data provided below.
* Drupal\uq_example\Service\ProgramService class
    * uq_example/src/Service/ProgramService.php
    * ProgramService is responsible for program related business logic.
    * method getProgramLevel(Program $program) that returns “Undergraduate” for
      type "U" programs, "Postgraduate" for type "P" and "PR",
      "Postgraduate Coursework" for type "PCW" programs, and "Other" for all
      other type codes.
    * method percentFormatter(string $value) that adds a ‘%’ to a string if
      numeric, otherwise returns the string as provided.
        * e.g. "%" should be appended to "0", "5", "10", "0.015"
          but not "<5", "N/A" or "N/P".
* Drupal\Tests\uq_example\Unit\ProgramServiceTest class (PHPUnit test class)
    * uq_example/tests/src/Unit/Service/ProgramServiceTest.php
    * testProgramLevel()
    * testPercentFormatter()
    * use of PHPUnit DataProvider annotation and using PHPUnit Prophecy to
      provide a test double for the Program class is preferred.
        * https://phpunit.de/manual/4.8/en/writing-tests-for-phpunit.html#writing-tests-for-phpunit.data-providers
        * https://phpunit.de/manual/4.8/en/test-doubles.html
        * https://www.drupal.org/docs/8/phpunit/using-prophecy
* Drupal\uq_example\Controller\ProgramController class
    * uq_example/src/Controller/ProgramController.php
    * ProgramController is responsible for program pages.
    * provides static method programDetailContent($code)
        * returns a Drupal Render API render array for program details matching
          the supplied wireframe.
        * If the Program level is "Other" then don't show the Program level row.
* Menu hook (uq_example_menu) and required page callback function(s)
    * Serves url path - '/program/{code}'
    * Renders Drupal\uq_example\Controller\
      ProgramController::programDetailContent($code) content.

PHPUnit tests and phpcs tests must pass.


Sample program data:

```
  $program['2000']['code'] = '2000';
  $program['2000']['title'] = 'Bachelor of Arts';
  $program['2000']['type'] = 'U';
  $program['2000']['percent'] = '0.156';
  $program['2000']['faculties'] = [
    'Humanities and Social Sciences'
  ];

  $program['2231']['code'] = '2231';
  $program['2231']['title'] = 'Bachelor of Information Technology/Arts';
  $program['2231']['type'] = 'U';
  $program['2231']['percent'] = 'N/A';
  $program['2231']['faculties'] = [
    'Engineering, Architecture and Information Technology',
    'Humanities and Social Sciences'
  ];

  $program['5429']['code'] = '5429';
  $program['5429']['title'] = 'Master of Architecture';
  $program['5429']['type'] = 'PCW';
  $program['5429']['percent'] = '<5';
  $program['5429']['faculties'] = [
   'Engineering, Architecture and Information Technology'
  ];
```

Notes for sample data:

Available program types:

| Key | Label         |
|-----|---------------|
| U   | Undergraudate |
| PCW | Postgraduate  |

## Instructions

**To install the project dependencies:**

* Clone the repository.
* Run:
```
composer install
```

**To run PHPUnit Tests:**

* To run the tests:
```
./vendor/bin/phpunit
```

**To run PHP Code Sniffer**

* To run phpcs:
```
composer run-script lint .
```

**To test on a Drupal 7 site**

* Clone the repository to sites/all/modules/custom/uq_example
* Enable uq_example custom module
* Download and enable the registry_autoload contrib module.
* Download the registry_rebuild contrib module
* Run drush registry rebuild and clear cache for Drupal to automatically
  load classes with registry_autoload

```
drush rr
drush cc all
```

* Confirm classes are autoloading by verifying no errors occur when
  constructing a class within Drupal.
```
drush eval "new Drupal\uq_example\Service\ProgramService();"
```
