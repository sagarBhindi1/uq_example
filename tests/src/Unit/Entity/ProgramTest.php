<?php

namespace Drupal\Tests\uq_example;

use PHPUnit\Framework\TestCase;

/**
 * Tests Program class.
 *
 * @coversDefaultClass Drupal\uq_example\Entity\Program
 */
class ProgramTest extends TestCase {

  /**
   * Tests the program class by creating a stub of it.
   */
  public function testProgramStub() {
    // Prophesize the Program class.
    $prophecy = $this->prophesize('Drupal\uq_example\Entity\Program');

    // Create stubs.
    $prophecy->getCode()->willReturn('2000');
    $prophecy->getTitle()->willReturn('Program stub');
    $prophecy->getType()->willReturn('Other');
    $prophecy->getPercent()->willReturn('0.015%');
    $prophecy->getFaculties()->willReturn('BEL');

    // Reveal the prophecy.
    $programStub = $prophecy->reveal();

    $this->assertEquals('2000', $programStub->getCode());
    $this->assertEquals('Program stub', $programStub->getTitle());
    $this->assertEquals('Other', $programStub->getType());
    $this->assertEquals('0.015%', $programStub->getPercent());
    $this->assertEquals('BEL', $programStub->getFaculties());
  }

}
