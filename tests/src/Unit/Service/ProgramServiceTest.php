<?php

namespace Drupal\Tests\uq_example;

use Drupal\uq_example\Factory\ProgramFactory;
use Drupal\uq_example\Service\ProgramService;
use PHPUnit\Framework\TestCase;

/**
 * Tests ProgramService class.
 */
class ProgramServiceTest extends TestCase {

  protected $program;
  protected $programService;

  /**
   * Setup data that is required in each test.
   *
   * @setup
   */
  protected function setUp() {
    $this->program['2000']['code'] = '2000';
    $this->program['2000']['title'] = 'Dummy Undergraduate Level program';
    $this->program['2000']['type'] = 'U';
    $this->program['2000']['percent'] = '0.156';
    $this->program['2000']['faculties'] = [
      'Humanities and Social Sciences',
    ];

    $this->program['2231']['code'] = '2231';
    $this->program['2231']['title'] = 'Dummy Postgraduate Level program';
    $this->program['2231']['type'] = 'P';
    $this->program['2231']['percent'] = 'N/A';
    $this->program['2231']['faculties'] = [
      'Engineering, Architecture and Information Technology',
      'Humanities and Social Sciences',
    ];

    $this->program['5429']['code'] = '5429';
    $this->program['5429']['title'] = 'Dummy Postgraduate Coursework Level program';
    $this->program['5429']['type'] = 'PCW';
    $this->program['5429']['percent'] = '<5';
    $this->program['5429']['faculties'] = [
      'Engineering, Architecture and Information Technology',
    ];

    $this->program['1100']['code'] = '1100';
    $this->program['1100']['title'] = 'Dummy Other Level program';
    $this->program['1100']['type'] = 'O';
    $this->program['1100']['percent'] = '<5';
    $this->program['1100']['faculties'] = [
      'Engineering, Architecture and Information Technology',
    ];

    $this->programService = new ProgramService();
  }

  /**
   * Tests the getProgramLevel function.
   *
   * @dataProvider programCodes
   * @covers Drupal\uq_example\Service\ProgramService::getProgramLevel
   */
  public function testProgramLevel($code, $expected) {
    $programFactory = new ProgramFactory();
    $program = $programFactory->createProgram($this->program[$code]);
    $programLevel = $this->programService->getProgramLevel($program);

    $this->assertEquals($expected, $programLevel);
  }

  /**
   * Tests the percentFormatter function.
   *
   * @dataProvider programMerits
   * @covers Drupal\uq_example\Service\ProgramService::percentFormatter
   */
  public function testPercentFormatter($merit, $expected) {
    $programMerit = $this->programService->percentFormatter($merit);

    $this->assertEquals($expected, $programMerit);
  }

  /**
   * Data provider for testing different program levels.
   */
  public function programCodes() {
    return [
      "Undergraduate Level program test" => ["2000", "Undergraduate"],
      "Postgraduate Level program test" => ["2231", "Postgraduate"],
      "Postgraduate Coursework Level program test" => ["5429", "Postgraduate Coursework"],
      "Other Level program test" => ["1100", "Other"],
    ];
  }

  /**
   * Data provider for testing different program merits.
   */
  public function programMerits() {
    return [
      "Integer Value" => ["0", "0%"],
      "Decimal Value" => ["0.015", "0.015%"],
      "Non-numeric Value" => ["<5", "<5"],
      "String" => ["N/A", "N/A"],
    ];
  }

  /**
   * Unset variables that were initialized during setUp.
   *
   * @tearDown
   */
  protected function tearDown() {
    unset($this->programService);
    unset($this->program);
  }

}
