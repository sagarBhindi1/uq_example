Hi Greg,

I made the following assumptions while completing the exercise:

* We were meant to create our repository in BitBucket and commit to that repo.
* Program type is either in lowercase or uppercase
* Any linting errors from the code that was supplied were to be ignored.

Kind Regards,
Sagar
