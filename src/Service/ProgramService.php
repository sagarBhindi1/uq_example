<?php

namespace Drupal\uq_example\Service;

use Drupal\uq_example\Entity\Program;

/**
 * Program related business logic.
 */
class ProgramService {

  /**
   * Get program level.
   *
   * @param \Drupal\uq_example\Entity\Program $program
   *   Program data.
   *
   * @return string
   *   The program level.
   */
  public function getProgramLevel(Program $program) {
    if ($program->getType()) {
      if ($program->getType() === "U" || $program->getType() === "u") {
        return "Undergraduate";
      }
      elseif ($program->getType() === "P" || $program->getType() === "p" || $program->getType() === "PR" || $program->getType() === "pr") {
        return "Postgraduate";
      }
      elseif ($program->getType() === "PCW" || $program->getType() === "pcw") {
        return "Postgraduate Coursework";
      }
      else {
        return "Other";
      }
    }
    return "n/a";
  }

  /**
   * Adds a ‘%’ to a string if numeric, otherwise returns string as provided.
   *
   * @param string $value
   *   Value.
   *
   * @return string
   *   The formatted string.
   */
  public function percentFormatter($value) {
    return is_numeric($value) ? $value . "%" : $value;
  }

}
