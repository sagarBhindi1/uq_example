<?php

namespace Drupal\uq_example\Controller;

use Drupal\uq_example\Factory\ProgramFactory;
use Drupal\uq_example\Service\ProgramService;

/**
 * Class ProgramController.
 */
class ProgramController {

  /**
   * Returns a render array for program details.
   *
   * @param string $code
   *   Program code.
   *
   * @return array
   *   Render array containing program details.
   */
  public static function programDetailContent($code) {
    // Get the program data.
    $programData = self::getProgramData($code);

    // Convert the program data to a program object.
    if ($programData) {
      $programFactory = new ProgramFactory();
      $program = $programFactory->createProgram($programData);

      if ($program) {
        $programFaculties = implode(",", $program->getFaculties());
        $programService = new ProgramService();
        $programLevel = $programService->getProgramLevel($program);
        $programMerit = $programService->percentFormatter($program->getPercent());

        $rows = [
          ["Program title", $program->getTitle()],
          ["Program code", $program->getCode()],
          ["Faculty", $programFaculties],

        ];

        if ($programLevel !== "Other") {
          $rows[] = ["Program level", $programLevel];
        }

        $rows[] = ["Program Merit", $programMerit];
      }
      else {
        $rows = [
            ["Program information cannot be retrieved for the program code " . $code],
        ];
      }

      $render_array["program"] = [
        "#theme" => "table",
        "#rows" => $rows,
      ];

      return render($render_array);
    }
    else {
      // Return a 404 not found if the program is not found.
      return drupal_not_found();
    }
  }

  /**
   * Dummy program data.
   *
   * @param string $code
   *   Program code.
   *
   * @return array
   *   Array containing program details.
   */
  private static function getProgramData($code) {
    $program['2000']['code'] = '2000';
    $program['2000']['title'] = 'Bachelor of Arts';
    $program['2000']['type'] = 'U';
    $program['2000']['percent'] = '0.156';
    $program['2000']['faculties'] = [
      'Humanities and Social Sciences',
    ];

    $program['2231']['code'] = '2231';
    $program['2231']['title'] = 'Bachelor of Information Technology/Arts';
    $program['2231']['type'] = 'U';
    $program['2231']['percent'] = 'N/A';
    $program['2231']['faculties'] = [
      'Engineering, Architecture and Information Technology',
      'Humanities and Social Sciences',
    ];

    $program['5429']['code'] = '5429';
    $program['5429']['title'] = 'Master of Architecture';
    $program['5429']['type'] = 'PCW';
    $program['5429']['percent'] = '<5';
    $program['5429']['faculties'] = [
      'Engineering, Architecture and Information Technology',
    ];

    return array_key_exists($code, $program) ? $program[$code] : FALSE;
  }

}
