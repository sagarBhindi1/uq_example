<?php

namespace Drupal\uq_example\Entity;

/**
 * Responsible for program information.
 */
class Program implements ProgramInterface {
  /**
   * The program code.
   *
   * @var string
   */
  private $code;

  /**
   * The program title.
   *
   * @var string
   */
  private $title;

  /**
   * The program type.
   *
   * @var string
   */
  private $type;

  /**
   * The program percent.
   *
   * @var string
   */
  private $percent;

  /**
   * The program faculties.
   *
   * @var array
   */
  private $faculties;

  /**
   * Constructor.
   *
   * @param string $code
   *   The program code.
   * @param string $title
   *   The program title.
   * @param string $type
   *   The program type.
   * @param string $percent
   *   The program percent.
   * @param array $faculties
   *   The program faculties.
   */
  public function __construct($code, $title, $type, $percent, array $faculties) {
    $this->code = $code;
    $this->title = $title;
    $this->type = $type;
    $this->percent = $percent;
    $this->faculties = $faculties;
  }

  /**
   * Get the program `code`.
   *
   * @return string
   *   The program code.
   */
  public function getCode() {
    return $this->code;
  }

  /**
   * Get the program `title`.
   *
   * @return string
   *   The program title.
   */
  public function getTitle() {
    return $this->title;
  }

  /**
   * Get the program `type`.
   *
   * @return string
   *   The program type.
   */
  public function getType() {
    return $this->type;
  }

  /**
   * Get the program `percent`.
   *
   * @return string
   *   The program percent.
   */
  public function getPercent() {
    return $this->percent;
  }

  /**
   * Get the program `faculties`.
   *
   * @return array
   *   The program faculties.
   */
  public function getFaculties() {
    return $this->faculties;
  }

}
