<?php

namespace Drupal\uq_example\Entity;

/**
 * Interface for program.
 */
interface ProgramInterface {

  /**
   * Get the program `code`.
   *
   * @return string
   *   The program code.
   */
  public function getCode();

  /**
   * Get the program `title`.
   *
   * @return string
   *   The program title.
   */
  public function getTitle();

  /**
   * Get the program `type`.
   *
   * @return string
   *   The program type.
   */
  public function getType();

  /**
   * Get the program `percent`.
   *
   * @return string
   *   The program percent.
   */
  public function getPercent();

  /**
   * Get the program `faculties`.
   *
   * @return array
   *   The program faculties.
   */
  public function getFaculties();

}
