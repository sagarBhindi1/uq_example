<?php

namespace Drupal\uq_example\Factory;

use Drupal\uq_example\Entity\Program;

/**
 * Responsible for creating Program entity objects.
 */
class ProgramFactory implements ProgramFactoryInterface {

  /**
   * Create a program.
   *
   * @param array $data
   *   Program data.
   *
   * @return Drupal\uq_example\Entity\Program
   *   Program object.
   *
   * @throws InvalidArgumentException
   *   If the code isn't recognised.
   */
  public function createProgram(array $data) {
    try {
      return new Program($data['code'], $data['title'], $data['type'], $data['percent'], $data['faculties']);
    }
    catch (InvalidArgumentException $e) {
      var_dump("The program code isn't recognised. Exception: " . $e->getMessage());
    }
  }

}
