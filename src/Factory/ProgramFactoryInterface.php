<?php

namespace Drupal\uq_example\Factory;

/**
 * Interface for the program factory.
 */
interface ProgramFactoryInterface {

  /**
   * Create a program.
   *
   * @param array $data
   *   Program data.
   *
   * @return Drupal\uq_example\Entity\Program
   *   Program object.
   *
   * @throws InvalidArgumentException
   *   If the code isn't recognised.
   */
  public function createProgram(array $data);

}
